module "rdaes" {
  source = "./es"
  region = "$region"
  vpc_id = "vpc-1111111"
  vpc_cidr = "1.1.1.1/23"
  availability_zones = "zone-1a,zone-1b"
  private_subnet_ids = "subnet-111111,subnet-222222/c"
  key_name = "aws-watever"
}
